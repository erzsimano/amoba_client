package hu.bme.szarch.amoba.client.controller;

import hu.bme.szarch.amoba.client.view.MainView;
import hu.bme.szarch.amoba.client.view.TableView;

public class AppController {

	private TableView gameView;
	private MainView mainView;

	private MainModel mainModel;

	public AppController(MainView mainView, MainModel mainModel) {
		this.mainView = mainView;
		this.mainModel = mainModel;
	}

}

package hu.bme.szarch.amoba.client.controller;

import javax.swing.JPanel;

public abstract class ClientState {

	protected JPanel view;

	public abstract void onStateActivation();

	public abstract void onStateDeactivation();

	public JPanel getView() {
		return view;
	}
}

package hu.bme.szarch.amoba.client.controller;

import hu.bme.szarch.amoba.client.model.game.ClientGameModel;

public class MarkCommand implements Command {

	private ClientGameModel model;

	private int x, y;
	private short mark;

	public MarkCommand(ClientGameModel model, int x, int y, short mark) {
		this.model = model;
		this.x = x;
		this.y = y;
		this.mark = mark;
	}

	@Override
	public void execute() {
		model.mark(x, y, mark);
	}

}

package hu.bme.szarch.amoba.client.controller;

public interface Command {
	public void execute();
}


package hu.bme.szarch.amoba.client.app;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class IconLoader {
	private Map<Integer, BufferedImage> icons;
	private static IconLoader instance;

	private IconLoader() throws IOException {
		icons = new TreeMap<>();
	}

	public void init() throws IOException {
		List<URL> iconFileList = new ArrayList<>();

		String path = "/hu/bme/szarch/amoba/client/app/icons";
		for (int i = 0; i <= 2; i++) {
			String filePath = "";
			filePath = path + i;

			filePath += ".png";

			URL url = IconLoader.class.getResource(filePath);

			iconFileList.add(url);
		}

		for (int imageIdx = 0; imageIdx < iconFileList.size(); imageIdx++) {
			icons.put(imageIdx, ImageIO.read(iconFileList.get(imageIdx)));
		}
	}

	public Image getIcon(int id) {
		return icons.get(id);
	}

	public ImageIcon getImageIcon(int id) {
		Image icon = getIcon(id);
		if (icon == null)
			return null;

		return new ImageIcon(icon);
	}

	public int getIconCount() {
		return icons.size();
	}

	public Set<Integer> getIconIDs() {
		return icons.keySet();
	}

	public static IconLoader getInstance() {
		if (instance == null) {
			try {
				instance = new IconLoader();
			} catch (IOException e) {
				System.out.println("Cannot load images!");
				e.printStackTrace();
			}
		}
		return instance;
	}
}

package hu.bme.szarch.amoba.client.network;

import java.util.Map;

import hu.bme.szarch.amoba.common.network.MessageHandler;
import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.common.network.NetworkMessageRouter;

public class ClientMessageRouter implements NetworkMessageRouter {

	Map<String, MessageHandler> messageHandlers;

	@Override
	public void registerMessageHandler(MessageHandler handler) {
		messageHandlers.put(handler.getHandleId(), handler);
	}

	@Override
	public void routeMessage(NetworkMessage message) {
		MessageHandler handler = messageHandlers.get(message.getId());

		if (handler != null)
			handler.handleMessage(message);
	}

	@Override
	public void unregisterMessageHandler(MessageHandler handler) {
		messageHandlers.remove(handler);
	}

}

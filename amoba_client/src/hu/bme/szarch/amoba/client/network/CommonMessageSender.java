package hu.bme.szarch.amoba.client.network;

import hu.bme.szarch.amoba.common.network.MessageSender;
import hu.bme.szarch.amoba.common.network.NetworkMessage;

public interface CommonMessageSender extends MessageSender {

	public void sendMessage(String address, int port, NetworkMessage message);
}

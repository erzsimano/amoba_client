package hu.bme.szarch.amoba.client.view;

import java.awt.Point;
import java.awt.event.MouseListener;

import hu.bme.szarch.amoba.client.model.table.ClientTableModel;

public interface AmobaClientTableView {
	public void update(ClientTableModel model);

	public Point getSize(int width, int height);

	public void addMouseListener(MouseListener listener);
}

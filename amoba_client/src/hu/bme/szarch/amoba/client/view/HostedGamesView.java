package hu.bme.szarch.amoba.client.view;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import hu.bme.szarch.amoba.client.model.game.HostedGamesListModel;
import hu.bme.szarch.amoba.common.game.GameData;

public class HostedGamesView extends JPanel {
	private JTable table;
	private JButton btnJoin;
	private JButton btnRefresh;
	private JButton btnBack;

	private GameDataTableModel tableModel;

	public HostedGamesView() {
		super();
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblHostedGames = new JLabel("Hosted games");
		lblHostedGames.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_lblHostedGames = new GridBagConstraints();
		gbc_lblHostedGames.gridwidth = 3;
		gbc_lblHostedGames.insets = new Insets(0, 0, 5, 0);
		gbc_lblHostedGames.gridx = 0;
		gbc_lblHostedGames.gridy = 0;
		add(lblHostedGames, gbc_lblHostedGames);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		add(scrollPane, gbc_scrollPane);

		table = new JTable();

		tableModel = new GameDataTableModel();

		table.setModel(tableModel);
		table.setShowVerticalLines(false);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);

		btnBack = new JButton("Back");
		GridBagConstraints gbc_btnBack = new GridBagConstraints();
		gbc_btnBack.insets = new Insets(0, 0, 0, 5);
		gbc_btnBack.gridx = 0;
		gbc_btnBack.gridy = 2;
		add(btnBack, gbc_btnBack);

		btnRefresh = new JButton("Refresh");
		GridBagConstraints gbc_btnRefresh = new GridBagConstraints();
		gbc_btnRefresh.insets = new Insets(0, 0, 0, 5);
		gbc_btnRefresh.gridx = 1;
		gbc_btnRefresh.gridy = 2;
		add(btnRefresh, gbc_btnRefresh);

		btnJoin = new JButton("Join");
		GridBagConstraints gbc_btnJoin = new GridBagConstraints();
		gbc_btnJoin.gridx = 2;
		gbc_btnJoin.gridy = 2;
		add(btnJoin, gbc_btnJoin);
	}

	public void update(HostedGamesListModel model) {
		List<GameData> games = model.getGameList();

		tableModel.refreshList(games);
	}

	public void setBackButtonListener(ActionListener listener) {
		btnBack.addActionListener(listener);
	}

	public void setRefreshButtonListener(ActionListener listener) {
		btnRefresh.addActionListener(listener);
	}

	public void setJoinButtonListener(ActionListener listener) {
		btnJoin.addActionListener(listener);
	}

	public int getSelectedGameId() {
		int selectedRow = table.getSelectedRow();
		if (selectedRow > -1) {
			GameData gameData = tableModel.getGameDataAt(selectedRow);
			return gameData.getId();
		}

		return -1;
	}

	@SuppressWarnings("serial")
	private class GameDataTableModel extends DefaultTableModel {
		private List<GameData> games;

		String[] tableColumnNames = new String[] { "Host", "Table width", "Table height", "Win criteria" };

		public GameDataTableModel() {
			games = new ArrayList<>();
		}

		public void refreshList(List<GameData> games) {
			this.games.clear();
			this.games.addAll(games);
		}

		public GameData getGameDataAt(int idx) {
			return games.get(idx);
		}

		@Override
		public int getColumnCount() {
			return tableColumnNames.length;
		}

		@Override
		public int getRowCount() {
			return games.size();
		}

		@Override
		public Object getValueAt(int row, int column) {
			GameData game = games.get(row);

			switch (column) {
			case 0:
				return game.getHost();
			case 1:
				return game.getWidth();
			case 2:
				return game.getHeight();
			case 3:
				return game.getWinCriteria();
			}

			return null;
		}

	}
}

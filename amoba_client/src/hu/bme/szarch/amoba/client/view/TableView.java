package hu.bme.szarch.amoba.client.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import hu.bme.szarch.amoba.client.app.IconLoader;
import hu.bme.szarch.amoba.client.model.table.ClientTableModel;
import hu.bme.szarch.amoba.common.exception.InvalidTableFieldException;

public class TableView extends JPanel {
	public interface MarkListener {
		public void onMark(int x, int y);
	}

	private int xOffset;

	private int yOffset;

	private int cellSize = 40;

	private Dimension panelSize;

	private ClientTableModel model;

	private MarkListener markListener;

	public TableView() {
		super();

		MouseAdapter mouseAdapter = new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				Point point = e.getPoint();

				Point normalizedCoords = getCellFromCoords(point);

				if (markListener != null) {
					markListener.onMark(normalizedCoords.x, normalizedCoords.y);
				}
			}
		};

		addMouseListener(mouseAdapter);
	}

	public Point getCellFromCoords(Point point) {
		Point retVal = new Point();

		int xMax = (int) ((getSize().getWidth() - xOffset));
		int yMax = (int) ((getSize().getHeight() - yOffset));

		if (point.x > -xOffset && point.x < xMax && point.y > yOffset && point.y < yMax) {
			retVal.x = (point.x - xOffset) / cellSize;
			retVal.y = (point.y - yOffset) / cellSize;
		} else {
			retVal.x = retVal.y = -1;
		}

		return retVal;
	}

	public void drawTable(Graphics g) throws InvalidTableFieldException {
		Graphics2D g2d = (Graphics2D) g.create();

		int rowCount = model.getRowCount();
		int columnCount = model.getColumnCount();

		g2d.setBackground(Color.WHITE);

		g2d.setColor(Color.BLACK);

		g2d.translate(xOffset, yOffset);

		g2d.clearRect(0, 0, columnCount * cellSize, rowCount * cellSize);

		for (int row = 0; row < rowCount; row++) {
			int cellY = row * cellSize; //

			for (int col = 0; col < columnCount; col++) {
				int cellX = col * cellSize; //

				g2d.drawRect(cellX, cellY, cellSize, cellSize);

				if (model.getMarkAt(col, row) != -1) {
					g2d.drawImage(IconLoader.getInstance().getIcon(model.getMarkAt(col, row)), cellX + 1, cellY + 1,
							cellSize - 1, cellSize - 1, null);
				}
			}
		}

		g2d.dispose();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		panelSize = getSize();

		try {
			drawTable(g);
		} catch (InvalidTableFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void adjustPanelSizeToTable(ClientTableModel model) {
		int newW = (int) (model.getColumnCount() * cellSize + 40);
		int newH = (int) (model.getRowCount() * cellSize + 40);
		panelSize.width = getParent().getWidth() > newW ? getParent().getWidth() : newW;
		panelSize.height = getParent().getHeight() > newH ? getParent().getHeight() : newH;
		this.setSize(panelSize);
		this.setPreferredSize(panelSize);
		this.setMinimumSize(panelSize);

		xOffset = (int) ((getWidth() - (model.getColumnCount() * cellSize)) / 2.0);
		yOffset = (int) ((getHeight() - (model.getRowCount() * cellSize)) / 2.0);
	}

	public void setMarkListener(MarkListener listener) {
		this.markListener = listener;
	}

	public void update(ClientTableModel model) {

		this.model = model;

		int columnCount = model.getColumnCount();
		int rowCount = model.getRowCount();

		adjustPanelSizeToTable(model);

		xOffset = (int) ((getWidth() - (columnCount * cellSize)) / 2.0);
		yOffset = (int) ((getHeight() - (rowCount * cellSize)) / 2.0);

		invalidate();
		repaint();
	}
}

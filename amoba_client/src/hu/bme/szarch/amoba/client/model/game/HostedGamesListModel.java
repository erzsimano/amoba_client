package hu.bme.szarch.amoba.client.model.game;

import java.util.ArrayList;
import java.util.List;

import hu.bme.szarch.amoba.client.view.HostedGamesView;
import hu.bme.szarch.amoba.common.game.GameData;

public class HostedGamesListModel {

	protected List<HostedGamesView> views;
	protected List<GameData> gameList;

	public HostedGamesListModel() {
		views = new ArrayList<>();
	}

	protected void updateViews() {
		for (HostedGamesView view : views)
			view.update(this);
	}

	public void addView(HostedGamesView view) {
		views.add(view);
	}

	public void removeView(HostedGamesView view) {
		views.remove(view);
	}

	public void setHostedGames(List<GameData> gameList) {
		this.gameList.clear();
		this.gameList.addAll(gameList);
		updateViews();
	}

	public List<GameData> getGameList() {
		return gameList;
	}
}

package hu.bme.szarch.amoba.client.model.game;

import hu.bme.szarch.amoba.client.controller.Command;

public interface ClientGameModel {

	public void executeCommand(Command command);

	public void mark(int x, int y, short mark);
}

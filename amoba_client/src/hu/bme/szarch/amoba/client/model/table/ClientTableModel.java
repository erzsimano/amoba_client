package hu.bme.szarch.amoba.client.model.table;

import hu.bme.szarch.amoba.client.view.AmobaClientTableView;
import hu.bme.szarch.amoba.common.table.GameTableModel;

public interface ClientTableModel extends GameTableModel {
	public void addView(AmobaClientTableView view);

	public void removeView(AmobaClientTableView view);
}
